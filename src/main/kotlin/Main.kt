import java.io.File
import java.nio.file.Path
import java.time.LocalDate
import kotlin.io.path.*

/*
    Application: Gestió d'usuaris 2022

    Description: Volem implementar el sistema d’alta i gestió d’usuaris d’un gimnàs del grup ASMA. Aquest nou sistema permetrà donar d’alta a nous usuaris
    així com enregistrar canvis sobre aquests. Cal tenir en compte que actualment ja existeix un sistema i caldrà implementar certes funcionalitats per poder
    rebre informació del sistema antic en fitxer d’un format concret.

    Options:
    El projecte constà dùn menú amb les següents opcions: IMPORTAR, CREAR, MODIFICAR, DES/BLOQUEJAR i ELIMINAR USUARI. Crear un BACKUP i SORTIR.
    Aquesta informació es guardaran en 3 directoris: IMPORT, BACK UP i DATA
     - IMPORTAR: es podran importar els usuaris antics i incloure'ls al nou arxiu 'userData'.
     - CREAR: es podran crear usuaris que s'inclouran a l'arxiu 'userData'
     - MODIFICAR: es podran modificar les dades dels usuaris a la
     - DES/BLOQUEJAR: es podrà desbloquejar i bloquejar l'estat de l'usuari a la base de dades 'userData'

    > BACKUP
    De la informació recaptada durant el dia es podrà realitzar un BACKUP, aquest es podrà realitzar manualment en qualsevol moment o si aquest no ha sigut
    realitzat manualment, es realitzaran de forma automàtica en SORTIR del programa

    Author: Martí Gustamante & Sergio González
    Start date: 2023, January 30th
 */



fun main() {

    // Cridem la funció init per crear els directoris que necessitarem
    init()

    // En iniciar el programa s'importen els arxius de la carpeta Import
    importarArxius()

    // Inicialitzem la variable sortir com a falsa, serà la que ens indicarà quan sortir del bucle
    var sortir = false

    // Bucle principal del programa
    do {

        // Es mostra el menú
        mostrarMenu()

        // S'avalua l'opció introduïda
        when (readln()) {
            "1" -> {
                existenciaArxius()
                importarArxius()
            }

            "2" -> {
                crearUsuari()
            }

            "3" -> {
                modificarUsuari()
            }

            "4" -> {
                desBloquejarUsuari()
            }

            "5" -> {
                eliminarUsuari()
            }

            "6" -> {
                guardarBackup()
            }

            "0" -> {
                sortir = true
            }

            else -> {
                println("Introdueix un valor correcte")
            }
        }
        println()
    } while (!sortir)

    // Es comprova si s'ha fet una còpia de seguretat aquest dia
    comprovarBackup()
}


/**
 * Funció que mostra les opcions del menú
 */
fun mostrarMenu() {
    println("###########################################")
    println("1. Importar dades")
    println("2. Crear usuari")
    println("3. Modificar usuari")
    println("4. Desbloquejar o Bloquejar usuaris")
    println("5. Eliminar usuari")
    println("6. Backup dades")
    println("0. EXIT")
    println("Tria una opció")
}


/**
 *  Funció que mira si hi ha arxius per importar o no.
 *  En cas positiu, importa els fitxers d'Import
 *  En cas negatiu, mostra un missatge d'error
 */
fun existenciaArxius() {

    // Mirem el directori Import
    val directoriImport = Path("./import/")

    if (directoriImport.exists()) {
        // Si el directori és buit, no hi ha fitxers per importar
        if (directoriImport.listDirectoryEntries().isEmpty()) {
            println("No hi ha fitxers per importar")

            // Si el directori no és buit, cridem la funció per importar tots els seus arxius
        } else {
            println("Important fitxers...si us plau esperi")
            importarArxius()
            println("Fitxers importats i eliminats del sistema")
        }
    }
}


/**
 * Funció que elimina un fitxer
 * @param file Camí del fitxer que es vol eliminar
 * @return Boolean que indica si el fitxer s'ha eliminat
 */
fun eliminarFitxer(file: Path): Boolean {
    return file.deleteIfExists()
}


/**
 * Funció que agafa els usuaris d'un fitxer antic i els envia a la funció per passar-los a usuaris nous
 * @param file Camí del fitxer del qual es volen agafar els usuaris antics
 */
fun importarArxiu(file: Path) {
//    println(file.toUri())
    val anticsUsuaris = retornUsuarisAntics(file)
    tractamentUsuaris(anticsUsuaris)

}


/**
 * Funció que, per cada fitxer de la carpeta Import, importa els seus usuaris i elimina el fitxer
 */
fun importarArxius() {

    // A partir del directori Import, agafem una llista amb tots els seus arxius
    val directori = Path("./import/")
    if (directori.exists()) {


        val arxius: List<Path> = directori.listDirectoryEntries()

        // Per cada arxiu, n'importem les dades i l'eliminem.
        for (arxiu in arxius) {
            importarArxiu(arxiu)
            eliminarFitxer(arxiu)
        }
    } else {
        println("David, has de crear el directori Import, crack.")
    }
}


/**
 * Funció que a partir d'un fitxer d'Import genera una llista amb els seus usuaris
 * @param path Camí del fitxer del qual importarem les dades
 */
fun retornUsuarisAntics(path: Path): MutableList<String> {

    // A partir del Path donat per paràmetre, en creem una variable File que serà amb la que operarem.
    val file = File(path.toUri())

    // Del fitxer agafem tot el contingut com a un String
    val textUsers = file.readText()

    // Del text, separem els usuaris en una llista sabent que el delimitador és ";", i ho convertim en un mutableList per
    // retornar-ho en un format més modificable.
    return textUsers.split(";").toMutableList()
}


/**
 * Funció que rep una llista dels usuaris antics com a String, amb camps separats per ",", canvia els separadors
 * per ";" i l'Id segons l'últim de userData. Finalment, guarda aquests usuaris a userData.
 * @param usuarisAntics Llista amb els usuaris rebuts d'algun fitxer de la carpeta Import.
 */
fun tractamentUsuaris(usuarisAntics: MutableList<String>) {

    // Agafem els usuaris "bons" de userData i els posem en un array per poder-ho manipular
    val userData = llegirUserData()

    // Iterem la llista dels usuaris antics
    for (usuari in usuarisAntics) {

        // De cada usuari en separem la informació
        val separaUsuari = usuari.split(",").toMutableList()
        var ultimID = 0

        // Si hi ha usuaris a userData, agafem l'Id de l'últim usuari
        if (userData.isNotEmpty()) {
            ultimID = userData.last().split(";")[0].toInt()
        }

        // A l'usuari antic actual li assignem un Id a partir del de l'últim usuari.
        separaUsuari[0] = (ultimID + 1).toString()

        // Tornem a passar les dades a String ajuntant-les amb el nou separador.
        val stringArreglat = separaUsuari.joinToString(";")

        // Si l'usuari antic està actiu i no bloquejat, el passem als usuaris nous.
        if (!(separaUsuari[4] == "false" && separaUsuari[5] == "true")) {
            userData.add(stringArreglat)
        }

    }

    // Registrem les dades al fitxer userData.
    guardarUserData(userData)
}


/**
 * Funció que crea un usuari a partir d'unes dades demanades i el guarda a userData.txt
 */
fun crearUsuari() {

    // Demanem les dades per crear el nou usuari
    println("Introdueix el nom del nou usuari")
    val nomUsuari = readln()
    println("Introdueix el telèfon del nou usuari")
    val telefonUsuari = readln()
    println("Introdueix el correu electrònic del nou usuari")
    val correuUsuari = readln()

    // Agafem els usuaris de userData i els posem en un array per poder-ho manipular
    val userData = llegirUserData()

    // Si hi ha usuaris a userData, agafem l'Id de l'últim usuari
    var ultimID = 0
    if (userData.isNotEmpty()) {
        ultimID = userData.last().split(";")[0].toInt()
    }

    // Afegim l'usuari a la llista
    userData.add("${(ultimID + 1)};$nomUsuari;$telefonUsuari;$correuUsuari;true;false")

    // Registrem les dades al fitxer userData.
    guardarUserData(userData)
}


/**
 * Funció que retorna l'índex d'un usuari dins de userData o -1 si no existeix
 * @param id L'Id de l'usuari que es vol trobar
 * @return L'índex de l'usuari en cas d'haver-lo trobat, o -1 si no existeix
 */
fun comprovaExistenciaUsuari(id: Int): Int {

    // Agafem els usuaris de userData i els posem en un array per poder-ho manipular.
    val userData = llegirUserData()

    // Iterem els índexs dels usuaris
    for (index in userData.indices) {
        // De l'usuari amb l'índex actual, agafem l'Id.
        val idUser = userData[index].split(";")[0].toInt()

        // Si l'Id de l'usuari coincideix amb l'Id buscat, retornem l'índex.
        if (idUser == id) return index
    }

    // Si no s'ha trobat l'Id, l'usuari no existeix: retornem -1.
    return -1
}


/**
 * Funció que modifica un usuari amb l'Id donat (si es troba)
 */
fun modificarUsuari() {

    // Demanem l'Id de l'usuari a modificar
    println("Quin és l'Id de l'usuari que vols modificar?")
    val idUser = readln().toInt()

    // Agafem l'índex de l'usuari a partir del seu Id
    val indexUser = comprovaExistenciaUsuari(idUser)

    // Agafem els usuaris de userData i els posem en un array per poder-ho manipular
    val userData = llegirUserData()

    // Si l'índex ens ha donat -1, mostrem missatge
    if (indexUser == -1) {
        println("No existeix cap usuari amb aquest id")

        // Si l'usuari s'ha trobat, el modifiquem
    } else {
        println("S'ha trobat l'usuari.")

        // Agafem la informació de l'usuari com a mutableList per poder-la modificar més fàcilment.
        val cosaAModificar = userData[indexUser].split(";").toMutableList()

        // Demanem les dades de l'usuari i modifiquem el mutableList.
        println("Introdueix el nou nom de l'usuari")
        cosaAModificar[1] = readln()
        println("Introdueix el nou telèfon de l'usuari")
        cosaAModificar[2] = readln()
        println("Introdueix el nou correu electrònic de l'usuari")
        cosaAModificar[3] = readln()

        // A la llista d'usuaris, substituïm l'usuari modificat per la nova informació.
        userData[indexUser] = cosaAModificar.joinToString(";")
    }

    // Registrem les dades al fitxer userData.
    guardarUserData(userData)
}


/**
 * Funció que elimina l'usuari amb l'Id donat (si es troba)
 */
fun eliminarUsuari() {

    // Demanem l'Id de l'usuari a eliminar
    println("Quin és l'Id de l'usuari que vols eliminar?")
    val idUser = readln().toInt()

    // Agafem l'índex de l'usuari a partir del seu Id
    val indexUser = comprovaExistenciaUsuari(idUser)

    // Agafem els usuaris de userData i els posem en un array per poder-ho manipular
    val userData = llegirUserData()

    // Si l'índex no s'ha trobat, mostrem missatge.
    if (indexUser == -1) {
        println("No existeix cap usuari amb aquest id")

        // Si l'índex s'ha trobat, eliminem l'usuari situat a aquest índex.
    } else {
        println("S'ha trobat l'usuari.")
        userData.removeAt(indexUser)
        println("L'usuari s'ha eliminat satisfactòriament (del registre)")
    }

    // Registrem les dades al fitxer userData.
    guardarUserData(userData)
}


/**
 * Funció que canvia l'estat de bloqueig de l'usuari amb l'Id donat (si es troba)
 */
fun desBloquejarUsuari() {

    // Demanem l'Id de l'usuari que volem modificar
    println("Quin és l'Id de l'usuari que vols bloquejar/desbloquejar?")
    val idUser = readln().toInt()

    // Agafem l'índex de l'usuari a partir del seu Id
    val indexUser = comprovaExistenciaUsuari(idUser)

    // Agafem els usuaris de userData i els posem en un array per poder-ho manipular
    val userData = llegirUserData()

    // Si l'índex no s'ha trobat, mostrem missatge.
    if (indexUser == -1) {
        println("No existeix cap usuari amb aquest id")

        // Si l'índex s'ha trobat, canviem el seu estat de bloqueig
    } else {
        println("S'ha trobat l'usuari.")

        // Separem les dades a modificar
        val usuari = userData[indexUser].split(";").toMutableList()

        // Modifiquem les dades depenent de l'estat de l'usuari
        if (usuari[5] == "true") {
            println("L'usuari s'ha desbloquejat satisfactòriament")
            usuari[5] = "false"

        } else {
            println("L'usuari s'ha bloquejat satisfactòriament")
            usuari[5] = "true"
        }

        // Substituïm les dades anteriors per les noves modificades
        userData[indexUser] = usuari.joinToString(";")
    }

    // Registrem les dades al fitxer userData
    guardarUserData(userData)
}


/**
 * Funció que genera una còpia de seguretat de "userData" amb la data d'avui
 */
fun guardarBackup() {
    val fitxerUserData = File("./data/userData.txt")

    val date = LocalDate.now()
    val backup = File("./backup/${date}userData.txt")

    // Fem una còpia del fitxer userData com a còpia de seguretat.
    fitxerUserData.copyTo(backup, true)
    println("Backup realitzat")
}


/**
 * Funció que comprova si existeix una còpia de seguretat amb la data d'avui, i en cas negatiu, el crea.
 */
fun comprovarBackup() {

    val date = LocalDate.now()
    val backup = File("./backup/${date}userData.txt")

    // Si no existeix una còpia de seguretat feta avui, cridem la funció per fer-ne una.
    if (!backup.exists()) {
        println("Realitzant un backup abans de sortir...")
        guardarBackup()
    }
}


/**
 * Funció que guarda les dades d'una llista al fitxer "userData.txt"
 * @param userData Llista amb les dades dels usuaris com a String, amb camps separats per ";"
 */
fun guardarUserData(userData: MutableList<String>) {
    val fitxerUserData = File("./data/userData.txt")
    // Esborrem el contingut del fitxer per sobreescriure-ho.
    fitxerUserData.writeText("")

    // A partir dels String de la llista userData, els escrivim al fitxer línia a línia.
    for (user in userData) {
        fitxerUserData.appendText("$user\n")
    }
}


/**
 * Funció que a partir del fitxer userData.txt genera una llista per a poder-ne modificar el contingut
 * @return Una llista amb les dades dels usuaris com a String, amb camps separats per ";"
 */
fun llegirUserData(): MutableList<String> {
    val userData = mutableListOf<String>()
    val fitxerUserData = File("./data/userData.txt")

    // Llegim el fitxer línia a línia i anem afegint el contingut a la llista.
    fitxerUserData.forEachLine { userData.add(it) }
    return userData
}

/**
 * Funció que crea els directoris i fitxers necessaris en cas que no existeixin, per evitar errors en el programa
 */
fun init(){
    var directori = Path("./backup/")
    if (!directori.exists()) {
        directori.createDirectory()
    }

    directori = Path("./import/")
    if (!directori.exists()){
        directori.createDirectory()
    }

    directori = Path("./data/")
    if (!directori.exists()){
        directori.createDirectory()
    }

    val fitxer = File("./data/userData.txt")
    if (!fitxer.exists()){
        fitxer.createNewFile()
    }
}
