# MANIPULACIÓ DE DADES

- [**Introducció**](#introducció)
- [**Objectius**](#objectiu)
- [**Descripció de l'implementació**](#descripcio)
- [**Funcionalitat**](#funcionalitat)
- [**Autors**](#autors)


## Introducció<a name="introducció"></a>

La **<span style="Color: red">manipulació de dades</span>** és una de les principals funcions informàtiques. Les dades són la informació que ens pot arribar de diferents maneres:
<br>

     - Ingrés manual: les dades es poden introduir directament mitjançant un teclat o un dispositiu d'entrada.

     - Fitxers: les dades es poden emmagatzemar en un fitxer al sistema i carregar-les en el moment necessari.

     - Dispositius externs: les dades es poden transferir des d'un dispositiu extern com una unitat flaix USB o un disc dur extern.

     - Xarxes: les dades es poden transferir mitjançant una xarxa, ja sigui local o en línia.

     - Captura automàtica: les dades es poden capturar automàticament mitjançant sensors o dispositius connectats al sistema.

     - Integració d'aplicacions: les dades es poden intercanviar entre aplicacions mitjançant APIs i serveis web.


## Objectius <a name="objectiu"></a>

Degut a la manipulació de dades i dels diferents camins per on ens poden arribar les següents dades volem implementar el sistema d’alta i gestió d’usuaris d’un gimnàs del grup ASMA. 
<br>

Aquest nou sistema permet donar d’alta a nous usuaris així com enregistrar canvis sobre aquests. 
<br>

Cal tenir en compte que actualment ja existeix un sistema i caldrà implementar certes funcionalitats per poder rebre informació del sistema antic en fitxer d’un format concret.
<br>

## Descripció de l'implementació <a name="descripcio"></a>

Consisteix a desenvolupar un sistema de gestió d’usuaris d’un gimnàs. Com s’ha explicat anteriorment, hi ha diverses parts del projecte, primerament, tractarem la importació de dades del sistema antic al nou sistema. 
<br>

Després, desenvoluparem tot el tractament de dades del nou sistema, i finalment desenvoluparem un sistema de backups.
<br>

El projecte ha de tenir 3 directoris addicionals que es definiran amb els següents noms:
<br>
   
    - import: directori que contindrà els fitxers que hem d’importar del sistema antic.
    - backups: directori amb els fitxers de backup
    - data: directori amb el fitxer userData


## Funcionalitat<a name="funcionalitat"></a>

El sistema de gestió creat consta de les següents parts:

    1. Importar dades
    2. Crear usuari
    3. Modificar usuari
    4. Desbloquejar o Bloquejar usuaris
    5. Eliminar usuari
    6. Backup dades
    0. EXIT

En aquest menú es tenen les opcions en les que ens basarem per tal de tenir les opcions *import, backup* i *gestió de dades* de les antigues i de les noves dades que arribaran al nou programa de gestió ASMA.

El sistema de gestió de les dades ens garanteix que podem importar les dades en qualsevol moment per tal de si les bases de dades es van implementant amb diferent temporalitat.

El programa constarà d'una part on hagi un procés de que consulti el directori, previament creat, *import* comprovant si existeix algun fitxer per carregar dins del sistema amb dades antigues i es reformin de tal manera que s'introdueixin dins del nou sistema de gestió.

A part de poder *importar* les dades antigües es podran crear noves dades amb l'opció de *crear usuari* i *modificar dades*, *desbloquejar/bloquejar dades* o *eliminar usuaris* tant de les dades noves creades com de les dades que prèviament han estat importades per tal de tenir una gestió més controlada de les dades.

Com a exemple, la informació importada de l'antiga base de dades aparaixerà en una mateixa línia i en un fitxer de següent manera:

    12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;123FG34,Lara Gomez,689953247,lara1985@mail.com,true,true

On cada informació acabarà tenint un tag que corresponen a la següent informació:

    - id: 12732461
    - nom: Àngel Sanz
    - telèfon: 697543215
    - email: angel.sanz@mail.com
    - actiu: true
    - bloquejat/desbloquejat: false

En el cas de fer l'import d'antics usuaris, el sistema de gestió els haura d'incloure dintre de la nova llista que es crearà novament on ja estàn inclosos els nous usuaris creats fent que el *id antic* es transformi en el *nou id* que utilitzarem per a la nova base de dades, un número enter entre el 1 i infinit, amb una increment d'1, on l'antic usuari que s'inclourà tindrà com a *nou id* el següent número que correspongui a la linia de la llista del fitxer de forma automàtica.

Com a exemple, la informació una vegada transformada quedarà com:

    - id: 23 (automàtic)
    - nom: Àngel Sanz
    - telèfon: 697543215
    - email: angel.sanz@mail.com
    - actiu: true
    - bloquejat/desbloquejat: false

I serà introduïda al nou arxiu de la següent manera:

    - 23;Àngel Sanz;697543215;angel.sanz@mail.com;true;false

On cada camp d'usuari serà separat amb punt i coma (;) i cada usuari serà enregistrat en una nova linia de l'arxiu.

Com a exemple quedaria aixi:

    - 22;Marti Gustamante;34453243;marti.topg@mail;true;true
    - 23;Àngel Sanz;697543215;angel.sanz@mail.com;true;false
    - 24;Lara Gomez;689953247;lara1985@mail.com;true;true

Durant la creació d'usuari se li demanarà a l'usuari la informació necessaria per tal d'omplir tot allò necessari per a la base de dades, i que prèviament hem comentat amb l'importació, i que es la següent:

    - id: (automàtic)
    - nom: ...
    - telèfon: ...
    - email: ...

Per tal de tenir un control personal dels usuaris, qui controlen la creació d'usuaris hauràn de controlar la part d'activitat i des/bloqueig:

    - actiu: true/false
    - bloquejat/desbloquejat: true/false

Un cop importat o crear usuaris, per tal de modificar un usuari s’haurà d’introduïr la id d’usuari que vol ser modificat i les dades personals del mateix (nom, telèfon, mail). Si existeix un usuari amb aquell id, es carregaran totes les dades del fitxer, es modificarà i es tornarà a  sobrescriure tot el fitxer amb les dades actualitzades, en cas contrari, s’informarà que no existeix aquell usuari. 

I per tal de des/bloquejar un usuari es farà igual al de modificar usuari, s’introdueix un id i si aquest és correcte es carregaran totes les dades del fitxer, es modificarà el booleà de bloquejat i es tornarà a sobreescriure tot el fitxer amb les dades actualitzades, en cas contrari, s’informarà que no existeix aquell usuari. 

Per tal d'eliminar usuaris, agafarem tota la informació d'aquell usuari i l'eliminarem, i aquest id no podrà ser utilitzat per una nova informació, encara que es recomana no eliminar cap usuari.

Com exemple quedaria aixì:

    - 43; Àngel Sanz; 697543215; angel.sanz@mail.com; true; false;
    - 45; Lara Gomez; 689953247; lara1985@mail.com; true; true;

Tota aquesta informació tant com d'*importar* les dades antigues com de *crear usuari* i *modificar dades*, *desbloquejar/bloquejar dades* o *eliminar usuaris* es crearan sempre en un arxiu userData.txt on es guardarà tota la informació emmagatzemada durant aquest periode.

Per tant, el nom del fitxer de dades dels usuaris serà userData.txt i es guardarà al directori data, on cada usuari es registrarà en una nova línia del fitxer.

Per últim, implementem el sistema de backups, on només serà permès un al dia per tal de que no hi hagi problemes. Aquest fitxer es crearà a la carpeta *backup* on es sobreescriurà l'arxiu cada dia amb el següent format:

    AAAA-MM-DDuserData.txt

Tampoc volem que quedin dies sense backup, així doncs, ens assegurem que quan l’usuari esculli l’opció de sortir del programa, si no existeix cap backup, en farem un.

------------------------------------------------------------------
### Autors <a name="autors"></a>
- Martí Gustamente Clavell
- Sergio Gonzalez Junyent
